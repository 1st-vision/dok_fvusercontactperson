﻿.. |label| replace:: User-Ansprechpartner verwalten
.. |snippet| replace:: FvUserContactPerson
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.3.0
.. |maxVersion| replace:: 5.5.4
.. |version| replace:: 1.2.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Das Plugin ermöglicht es Ihnen, dem eingeloggten Kunden auf der "Mein Konto"-Seite und in der Kaufabwicklung seine Ansprechpartner mit Personendaten und Bild anzuzeigen.

Frontend
--------
Wenn ein Kunde sich einloggt, werden auf der "Mein Konto" - Seite die Kontakte angezeigt. Des Weiteren wird dies auch in der Kaufabwicklung angezeigt.

.. image:: FvUserContactPerson4.png

Backend
-------
Konfiguration
_____________
.. image:: FvUserContactPerson1.png

Hier hinterlegen Sie die Freitext-Felder, die beim Kunden hinterlegt sind. Diese Werte werden bei den Kontaktpersonen als Nummern bezeichnet. Alle Kunden die diese Nummer hinterlegt haben bekommen diese Kontaktperson angezeigt.
Für den automatischen Import von Daten steht ein Cronjob zur Verfügung welcher die Quelldatei im angegebenen Format importiert.
Über die anzugebende Mapping-ini wird angegebenen wie sich die XML-Attribute oder Spaltennamen der CSV auf die SQL-Tabellennamen übertragen, wie im Bild zu sehen ist.

.. image:: FvUserContactPerson3.png

zusätzlich dient der _meta-Abschnitt dazu die Datenbank-Tabelle zu definieren in die importiert werden soll und die Spalte die als unqiue-Key genutzt werden soll - also woran sich entscheidet ob ein Eintrag eingefügt oder aktualisiert wird.
Wird eine CSV importiert, so ist der Spaltentrenner wie gezeigt anzugeben.

Kontaktpersonen bearbeiten
__________________________
.. image:: FvUserContactPerson2.png
.. image:: FvUserContactPerson3.png

Die Texte wie Telefon etc. sind als Textbausteine hinterlegt und können so geändert und auch in den anderen Sprachen verändert/hinterlegt werden.

Textbausteine
_____________

Frontend
*********************
:FvUserContactPersonDetailPhone: Telefon:
:FvUserContactPersonDetailMobile: Mobil:
:FvUserContactPersonDetailFax: Fax:
:FvUserContactPersonDetailEmail: E-Mail:
:FvUserContactPersonDetailFeld1: Feld1
:FvUserContactPersonDetailFeld2: Feld2
:FvUserContactPersonDetailFeld3: Feld3
:FvUserContactPersonDetailFeld4: Feld4
:FvUserContactPersonDetailFeld5: Feld5
:FvUserContactPersonFrontendH1: Ihr persönlicher Ansprechpartner
:FvUserContactPersonQuestionsContactMe: Bei Fragen kontaktieren Sie mich

technische Beschreibung
------------------------
keine

Modifizierte Template-Dateien
-----------------------------
:/account/index.tpl:
:/checkout/confirm.tpl:


